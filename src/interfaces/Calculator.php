<?php

namespace Parking\Parking;

interface Calculator {
	public function set_open($hour, $min, $sec);
	public function get_open();
	public function set_close($hour, $min, $sec);
	public function get_close();
	public function set_price($price);
	public function get_price();
	public function set_double_first_hour($double_first_hour);
	public function get_double_first_hour();
	public function set_start($start);
	public function get_start();
	public function set_end($end);
	public function get_end();
	public function set_timezone($timezone);
	public function get_timezone();
	public function set_max_price_per_day($max_price_per_day);
	public function get_max_price_per_day();
	public function calculate();
}
