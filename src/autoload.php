<?php

spl_autoload_register(
	function ($class) {
		$classes = array(
			'parking\\parking\\parking' => '/Parking.php',
			'parking\\parking\\calculator' => '/interfaces/Calculator.php',
		);

		$cname = strtolower($class);
		if (isset($classes[$cname])) {
			require __DIR__ . $classes[$cname];
		}
	}
);
