<?php

namespace Parking\Parking;

// TODO: Time between closing and opening hours is always treated as a free time.
// Will there ever be a requirement to support many different hourly rates within the same parking session?
class Parking implements Calculator {
	/**
	 * @var mixed
	 */
	private $open;

	/**
	 * When the parking meter starts collecting money.
	 *
	 * @param int $hour The starting hour when we start collecting money.
	 * @param int $min The starting minute when we start collecting money.
	 * @param int $sec The starting second when we start collecting money.
	 * @return mixed Return the class context back to the user - for chaining purposes.
	 */
	public function set_open($hour, $min, $sec = 0) {
		$this->open = array(
			'hour' => $hour,
			'min' => $min,
			'sec' => $sec,
		);

		return $this;
	}

	/**
	 * Get opening hours.
	 *
	 * @return mixed Array of the "opens at" specification. Hour, minute and second as integers.
	 */
	public function get_open() {
		return $this->open;
	}

	/**
	 * @var mixed
	 */
	private $close;

	/**
	 * When the parking meter stops collecting money.
	 *
	 * @param int $hour The ending hour when we stop collecting money.
	 * @param int $min The ending minute when we stop collecting money.
	 * @param int $sec The ending second when we stop collecting money.
	 * @return mixed Return the class context back to the user - for chaining purposes.
	 */
	public function set_close($hour, $min, $sec = 0) {
		$this->close = array(
			'hour' => $hour,
			'min' => $min,
			'sec' => $sec,
		);

		return $this;
	}

	/**
	 * Get closing hours.
	 *
	 * @return mixed Array of the "closes at" specification. Hour, minute and second as integers.
	 */
	public function get_close() {
		return $this->close;
	}

	/**
	 * @var int
	 */
	private $price;

	/**
	 * Set the hourly price.
	 *
	 * @param int $price The hourly price/rate.
	 * @return mixed Return the class context back to the user - for chaining purposes.
	 */
	public function set_price($price) {
		$this->price = $price;

		return $this;
	}

	/**
	 * Get the hourly price rate.
	 *
	 * @return int The hourly price rate.
	 */
	public function get_price() {
		return $this->price;
	}

	/**
	 * @var bool
	 */
	private $double_first_hour;

	/**
	 * Double the first hour or not.
	 *
	 * @param bool $double_first_hour Double the payment for the first hour.
	 * @return mixed Return the class context back to the user - for chaining purposes.
	 */
	public function set_double_first_hour($double_first_hour) {
		$this->double_first_hour = $double_first_hour;

		return $this;
	}

	/**
	 * Double the first hour or not.
	 *
	 * @return bool True if the first hour should be doubled, false otherwise.
	 */
	public function get_double_first_hour() {
		return $this->double_first_hour;
	}

	/**
	 * @var int
	 */
	private $start;

	/**
	 * Set the start time, when the customer "clocks in" to the parking spot.
	 *
	 * @param int $start Unix timestamp representing the start of the parking session.
	 * @return mixed Return the class context back to the user - for chaining purposes.
	 */
	public function set_start($start) {
		$this->start = $start;

		return $this;
	}

	/**
	 * Get the parking session start time as unix timestamp.
	 *
	 * @return int Start of the parking session as unix timestamp.
	 */
	public function get_start() {
		return $this->start;
	}

	/**
	 * @var int
	 */
	private $end;

	/**
	 * Set the end time, when the customer "clocks out" of the parking spot.
	 *
	 * @param int $end Unix timestamp representing the end of the parking session.
	 * @return mixed Return the class context back to the user - for chaining purposes.
	 */
	public function set_end($end) {
		$this->end = $end;

		return $this;
	}

	/**
	 * Get the parking session end time as unix timestamp.
	 *
	 * @return int End of the parking session as unix timestamp.
	 */
	public function get_end() {
		return $this->end;
	}

	/**
	 * @var string
	 */
	private $timezone;

	/**
	 * Set the timezone that should be used.
	 *
	 * @param string $timezone One of the timezones defined at https://secure.php.net/manual/en/timezones.php.
	 * @return mixed Return the class context back to the user - for chaining purposes.
	 */
	public function set_timezone($timezone) {
		$this->timezone = $timezone;

		return $this;
	}

	/**
	 * Get a string representation of the timezone we are using.
	 *
	 * @return string The timezone we are using.
	 */
	public function get_timezone() {
		return $this->timezone;
	}

	/**
	 * @var int
	 */
	private $max_price_per_day;

	/**
	 * Set the maximum price per parking spot per day.
	 *
	 * @param int $max_price_per_day The maximum price as an integer.
	 * @return mixed Return the class context back to the user - for chaining purposes.
	 */
	public function set_max_price_per_day($max_price_per_day) {
		$this->max_price_per_day = $max_price_per_day;

		return $this;
	}

	/**
	 * Get the maximum price we can possibly charge the customer per calendar day.
	 *
	 * @return int The maximum price we can possibly charge per calendar day.
	 */
	public function get_max_price_per_day() {
		return $this->max_price_per_day;
	}

	/**
	 * Construct.
	 *
	 * @param int $price The hourly price.
	 * @param bool $double_first_hour Double the first hour or not.
	 * @param string $timezone One of the timezones defined at https://secure.php.net/manual/en/timezones.php.
	 */
	public function __construct($price, $double_first_hour = false, $timezone = 'Europe/Stockholm') {
		$this->price = $price;
		$this->double_first_hour = $double_first_hour;
		$this->timezone = $timezone;
	}

	/**
	 * The callable calculation method.
	 *
	 * @return int The price to pay.
	 */
	public function calculate() {
		if (empty($this->start) || empty($this->end) || empty($this->open) || empty($this->close)) {
			throw new RuntimeException("Missing one or more of the required arguments: start, end, open, close");
		}

		// Create \DateTime objects for the start and end times of the parking session.
		// This is the whole session, so there might be lots of minutes the customer doesn't need to pay for.
		$start_datetime = $this->create_datetime($this->start);
		$end_datetime = $this->create_datetime($this->end);

		// We break down the whole time interval into an array of days with chargable start and end timestamps.
		// This makes it much more easier to work with the data.
		$days = $this->segment_to_days($start_datetime, $end_datetime);

		$chargable_price = 0;
		foreach ($days as $day) {
			// Only double the first hour if $chargable_price is still 0.
			$chargable_price += $this->calculate_single_day($day, $chargable_price === 0);
		}

		return $chargable_price;
	}

	/**
	 * Calculate how much the customer has to pay for a single day.
	 *
	 * @param mixed $day An array with information when the charging stars and when it ends.
	 * @param bool $double_first_hour Double or not the first hour of the parking session.
	 */
	private function calculate_single_day($day, $double_first_hour = false) {
		// Create \DateTime objects from the actual start and end dates of the charging.
		$start_charge_datetime = $this->create_datetime($day['charging_starts_at']);
		$end_charge_datetime = $this->create_datetime($day['charging_ends_at']);

		$chargable_price = 0;
		// The interval is used to help out with calculating how many hours, minutes, etc. the customer will be charged for.
		$interval = $start_charge_datetime->diff($end_charge_datetime);
		// Calculate the price for all the hours parked during this specific day.
		if ($interval->h > 0) {
			$chargable_price += $interval->h * $this->price;
		}

		// Calculate the price for all the minutes parked during this specific day.
		if ($interval->i > 0) {
			// Price per minute is the hourly price divided by 60.
			$price_per_min = $this->price / 60;
			$chargable_price += $interval->i * $price_per_min;
		}

		if ($double_first_hour) {
			if ($interval->h > 0) {
				// Add an extra hourly price if the first hour should be doubled.
				$chargable_price += $this->price;
			} else {
				/*
					We double the whole amount if we have less than an hour to charge for.

					TODO: Should the doubling of the "first hour" extend between days?
					For example. The parking closes at 18:00. John Smith parks at 17:30, leaves the car for the night
					and picks it up again 2 hours after the opening hours. Currently we only double charge the first 30 minutes.
					We could extend the double charging if it's less than an hour, so the first 30 minutes of the second day are also double charged.
				*/

				// 0.5 added to make sure people never pay less than 1 SEK (that is, if they are charged at all).
				// However, that amount should be doubled since it's part of the "first hour".
				$chargable_price = round(($chargable_price + 0.5)) * 2;
			}
		}

		// Apply the maximum per day charge if applicable.
		if (!empty($this->max_price_per_day) && $chargable_price > $this->max_price_per_day) {
			$chargable_price = $this->max_price_per_day;
		}

		// We always ceil up to the next integer - just like every other shop does. :-P
		return intval(ceil($chargable_price));
	}

	/**
	 * Create and return an \DateTime object configured with the correct timezone and the provided timestamp.
	 *
	 * @param int $timestamp Unix timestamp to set the \DateTime object to.
	 * @return \DateTime Returns the initiated \DateTime object.
	 */
	private function create_datetime($timestamp) {
		$d = new \DateTime();
		$d->setTimezone(new \DateTimeZone($this->timezone));
		$d->setTimestamp($timestamp);

		return $d;
	}

	/**
	 * Segment the whole time period down into days with chargable time only.
	 *
	 * @param \DateTime $start The parking session start time as an \DateTime object.
	 * @param \DateTime $end The parking session end time as an \DateTime object.
	 * @return mixed Array with days and their chargable time only.
	 */
	private function segment_to_days($start, $end) {
		if (empty($start) || empty($end) || empty($this->open) || empty($this->close)) {
			throw new RuntimeException("Missing one or more of the required arguments: start, end, open, close");
		}

		// The resulting array of days.
		$days = array();

		// Little fix to make sure we don't leave out days that span less than 24 hours.
		$start_earliest = $this->create_datetime($start->getTimestamp());
		$start_earliest->setTime(0, 0, 0);
		$end_latest = $this->create_datetime($end->getTimestamp());
		$end_latest->setTime(23, 59, 59);
		$number_of_days = $start_earliest->diff($end_latest)->days + 1;
		for ($i = 0; $i < $number_of_days; $i++) {
			// Start and end timestamps for this specific day. Used multiple times in this code block.
			$start_timestamp = $start->getTimestamp();
			$end_timestamp = $end->getTimestamp();

			// \DateTime object for the day we are working with.
			// We multiply $start_timestamp with 86400 seconds (24 hours) times $i (starts at 0).
			$d = $this->create_datetime($start_timestamp + (86400 * $i));

			// Open and close timestamps for this specific day. Used multiple times in this code block.
			$open_timestamp = $d->setTime($this->open['hour'], $this->open['min'], $this->open['sec'])->getTimestamp();
			$close_timestamp = $d->setTime($this->close['hour'], $this->close['min'], $this->close['sec'])->getTimestamp();

			// Start and end timestamps that the customer will be charged for. By default it's the whole day (from open to close).
			$s = $open_timestamp;
			$e = $close_timestamp;

			$first_iter = $i === 0;
			if ($first_iter && $start_timestamp >= $close_timestamp) {
				// Skip to the next day if the customer "clocked in" to the parking spot after closure.
				continue;
			} else if ($first_iter && $start_timestamp > $open_timestamp) {
				// The customer shouldn't be charged from the opening hours since he/she "clocked in" to the parking spot after it was opened.
				$s = $start_timestamp;
			}

			$last_iter = ($i + 1) === $number_of_days;
			if ($last_iter && $end_timestamp < $open_timestamp) {
				// Skip this day if it's the last day and the customer "clocks out" before the parking was opened.
				break;
			} else if ($last_iter && $end_timestamp < $close_timestamp) {
				// The customer shouldn't be charged until the closing hours since he/she "clocked out" of the parking spot before it was closed.
				$e = $end_timestamp;
			}

			// Only return the chargable times for each and every day the parking session spanned.
			$days[] = array(
				'charging_starts_at' => $s,
				'charging_ends_at' => $e,
			);
		}

		return $days;
	}
}
