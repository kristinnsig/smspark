CWD = $(shell pwd)
PHP_EXEC = $(shell which php)
WGET_EXEC = $(shell which wget)
PHPUNIT_URL = https://phar.phpunit.de/phpunit-old.phar
PHPUNIT_EXEC ?= $(CWD)/phpunit.phar
AUTOLOAD_FILE = $(CWD)/src/autoload.php
TESTS_DIR = $(CWD)/tests

all: deps test
	@echo "Building Parking"

test:
	@echo "Running Parking tests"
	@$(PHP_EXEC) $(PHPUNIT_EXEC) --bootstrap $(AUTOLOAD_FILE) $(TESTS_DIR)/*

deps:
	@echo "Downloading dependencies"
	@echo $(PHPUNIT_URL)
	@$(WGET_EXEC) -O $(PHPUNIT_EXEC) $(PHPUNIT_URL)
