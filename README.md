# Parking price calculation

## Requirements

This was developed with `PHP` 5.5.9 and `PHPUnit` 4.8.24.

`wget` is required for downloading dependencies (currently only `PHPUnit`).

## Execution

Run `make` to download the `PHPUnit` dependency (for `PHP` 5.5.9) and execute the tests.
