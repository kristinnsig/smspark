<?php

namespace Parking\Parking;

define('TIMESTAMP_MIDNIGHT', 1460246400);
define('TIMESTAMP_NOON', 1460282400);
define('TIMESTAMP_END_OF_DAY', 1460332799);

class ParkingTest extends \PHPUnit_Framework_TestCase {
	public function test_price_calculation_two_hours() {
		$p = new Parking(5, true);
		$p->set_open(9, 0);
		$p->set_close(18, 0);
		$p->set_start(TIMESTAMP_NOON);
		$p->set_end(TIMESTAMP_NOON + 7200); // 2 hours.

		$this->assertEquals(15, $p->calculate());
	}

	public function test_price_calculation_three_days_with_max_price() {
		$p = new Parking(5, true);
		$p->set_open(9, 0)
			->set_close(18, 0)
			->set_max_price_per_day(25)
			->set_start(TIMESTAMP_NOON)
			->set_end(TIMESTAMP_NOON + (86400 * 2)); // Today + 2 days into the future = 3 days.

		$this->assertEquals(65, $p->calculate());
	}

	public function test_price_calculation_three_days_without_max_price() {
		$p = new Parking(5, true);
		$p->set_open(9, 0)
			->set_close(18, 0)
			->set_start(TIMESTAMP_NOON)
			->set_end(TIMESTAMP_NOON + (86400 * 2)); // Today + 2 days into the future = 3 days.

		$this->assertEquals(95, $p->calculate());
	}

	public function test_price_calculation_three_days_with_max_price_uneven_hour() {
		$p = new Parking(5, true);
		$p->set_open(9, 0)
			->set_close(18, 0)
			->set_max_price_per_day(25)
			->set_start(TIMESTAMP_NOON)
			->set_end(TIMESTAMP_NOON + (86400 * 2) + 300); // Today + 2 days and 10 minutes into the future = 3 days and 10 minutes.

		$this->assertEquals(66, $p->calculate());
	}

	public function test_payment_one_minute() {
		$p = new Parking(5, true);
		$p->set_open(9, 0)
			->set_close(18, 0)
			->set_max_price_per_day(25)
			->set_start(TIMESTAMP_NOON)
			->set_end(TIMESTAMP_NOON + 60); // 1 minute.

		$this->assertEquals(2, $p->calculate());
	}

	public function test_start_without_paying_end_up_paying_20_min() {
		$p = new Parking(5, true);
		$p->set_open(9, 0)
			->set_close(18, 0)
			->set_max_price_per_day(25)
			->set_start(TIMESTAMP_NOON - (3600 * 3) - 1200) // 3 hours and 20 minutes from noon - 20 minutes before opening.
			->set_end(TIMESTAMP_NOON - (3600 * 3) + 1200); // 2 hours and 40 minutes from noon - 20 minutes after opening.

		$this->assertEquals(4, $p->calculate());
	}

	public function test_start_after_closing_pay_1_hour_20_min() {
		$p = new Parking(5, true);
		$p->set_open(9, 0)
			->set_close(18, 0)
			->set_max_price_per_day(25)
			->set_start(TIMESTAMP_NOON + (3600 * 7)) // 7 hours from noon - 1 hour since closure.
			->set_end(TIMESTAMP_NOON + (3600 * 22) + 1200); // 1 hour and 20 minutes after opening the day after.

		$this->assertEquals(12, $p->calculate());
	}

	public function test_no_charge() {
		$p = new Parking(5, true);
		$p->set_open(9, 0)
			->set_close(18, 0)
			->set_max_price_per_day(25)
			->set_start(TIMESTAMP_NOON + (3600 * 7)) // 7 hours from noon - 1 hour since closure.
			->set_end(TIMESTAMP_NOON + (3600 * 10) + 1200); // 10 hours and 20 minutes from noon - still closed.

		$this->assertEquals(0, $p->calculate());
	}
}
